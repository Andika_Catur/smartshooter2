package com.example.smartshooter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.io.IOException;
import java.io.InputStream;

public class SplashScreen extends AppCompatActivity {
    ImageView GambarGif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        GambarGif = (ImageView)findViewById(R.id.imageView);

        Glide.with(SplashScreen.this)
                // LOAD URL DARI LOKAL DRAWABLE
                .load(R.drawable.logo)
                .asGif()
                //PENGATURAN CACHE
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(GambarGif);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent pindah = new Intent(SplashScreen.this, MainMenuActivity.class);
                startActivity(pindah);
                finish();
            }
        }, 5000);


    }
}