package com.example.smartshooter;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class Credit extends AppCompatActivity {
    ImageView imageView;
    MediaPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Membuat tampilan selalu menyala jika activity aktif
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        imageView = (ImageView)findViewById(R.id.imageView4) ;

        Glide.with(Credit.this)
                // LOAD URL DARI LOKAL DRAWABLE
                .load(R.drawable.background)
                .asGif()
                //PENGATURAN CACHE
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);
        playsound();
    }
    private void playsound (){
        try{
            if(player.isPlaying()){
                player.stop();
                player.release();
            }
        }catch (Exception e){

        }
        player= MediaPlayer.create(this, R.raw.spacebacksound);
        player.setLooping(true);
        player.start();

    }
    @Override
    protected void onPause() {
        super.onPause();
        if (player != null){
            player.stop();
            if (isFinishing()){
                player.stop();
                player.release();
            }
        }
    }
}
