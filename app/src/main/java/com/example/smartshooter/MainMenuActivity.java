package com.example.smartshooter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView GambarGif,GambarGif2;
    private Button mPlay, mHighScore, mExit, mCredit;
    private MediaPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Membuat tampilan selalu menyala jika activity aktif
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // TAMBAHKAN INI
        GambarGif = (ImageView)findViewById(R.id.imageView4);
        GambarGif2 = (ImageView)findViewById(R.id.imageView2) ;

        Glide.with(MainMenuActivity.this)
                // LOAD URL DARI LOKAL DRAWABLE
                .load(R.drawable.background)
                .asGif()
                //PENGATURAN CACHE
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(GambarGif);

        Glide.with(MainMenuActivity.this)
                .load(R.drawable.shop)
                .asGif()
                //PENGATURAN CACHE
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(GambarGif2);

        mPlay = findViewById(R.id.play);
        mHighScore = findViewById(R.id.high_score);
        mCredit = findViewById(R.id.credit);
        mExit = findViewById(R.id.exit);

        mPlay.setOnClickListener(this);
        mHighScore.setOnClickListener(this);
        mCredit.setOnClickListener(this);
        mExit.setOnClickListener(this);

        playsound();
    }
    private void playsound (){
        try{
            if(player.isPlaying()){
                player.stop();
                player.release();
            }
        }catch (Exception e){

        }
        player=MediaPlayer.create(this, R.raw.spacebacksound);
        player.setLooping(true);
        player.start();

    }
    @Override
    protected void onPause() {
        super.onPause();
        if (player != null){
            player.stop();
            if (isFinishing()){
                player.stop();
                player.release();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.play:
                startActivity(new Intent(this, GameActivity.class));
                finish();
                break;
            case R.id.high_score:
                startActivity(new Intent(this, Credit.class));
                break;
            case R.id.credit:
                startActivity(new Intent(this, Credit.class));
                break;
            case R.id.exit:
                finish();
                break;
        }
    }

}

